package io.highcreeksoftware.smokesignal.email

data class EmailAddress(val name: String, val address: String)