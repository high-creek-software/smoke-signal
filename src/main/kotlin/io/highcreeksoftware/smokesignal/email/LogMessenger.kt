package io.highcreeksoftware.smokesignal.email

class LogMessenger: EmailMessenger {
    override fun send(to: EmailAddress, from: EmailAddress, message: EmailMessage) {
        println("To: ${to.name} <${to.address}>")
        println("From: ${from.name} <${from.address}>")
        println(message.plain)
        println("----------")
        println(message.html)
    }
}