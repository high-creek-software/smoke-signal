package io.highcreeksoftware.smokesignal.email

data class EmailMessage(val subject: String, val plain: String?, val html: String?)