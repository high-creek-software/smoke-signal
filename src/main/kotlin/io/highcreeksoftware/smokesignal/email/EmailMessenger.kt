package io.highcreeksoftware.smokesignal.email

interface EmailMessenger {
    fun send(to: EmailAddress, from: EmailAddress, message: EmailMessage)
}